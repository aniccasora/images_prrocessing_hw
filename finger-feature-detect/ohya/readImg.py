from pathlib import Path

def readImg():
    # 檢查資料夾是否存在?
    if not Path('./images').exists():
        raise Exception("\'images\' folder NOT exists !!")
    if not Path('./output').exists():
        raise Exception("\'output\' folder NOT exists !!")

    # 圖片儲存的路徑。
    images_path = Path('./images').glob('*.*')

    # 將該資料夾(./images)下的 所有 "檔案名" 存至 -> images_path_list.
    images_path_list = [str(image_path) for image_path in images_path]

    # 紀錄 img name，檔名。
    img_name_list = [pth[pth.rfind('\\') + 1:] for pth in images_path_list]

    # zip (檔名 && 路徑)
    img_zip = list(zip(img_name_list, images_path_list))

    return img_zip