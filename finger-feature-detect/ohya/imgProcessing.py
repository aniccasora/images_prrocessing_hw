#
# If your are English user please don't watch this source code!
#
import numpy as np
import cv2


# 檢測 黃種人的膚色!!
# return : 認為是黃色皮膚的區域~
def detect_NON_nigger_Skin(img: np.ndarray, race = "asia"):
    if str(race).lower() == "asia":
        # 用 HSV 來界定 黃種人膚色。
        lower = np.array([0, 18, 80], dtype="uint8")
        upper = np.array([30, 255, 255], dtype="uint8")

        # 轉成 HSV 色彩空間, 才能調用 inrange
        hsv_img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        # inRange(hsv, lower, upper), 低於 loser,大於 upper 都設=0
        skin_mask = cv2.inRange(hsv_img, lower, upper)
        # 先拿到一個'遮罩'，之後用 此遮罩，再做 侵蝕 -> 擴張。
        # 目的是因為，你現在拿到的 skin_mask，有一些的點點，所以要先侵蝕將其弄消失。
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(5,5))
        # 再做 侵蝕 -> 擴張。(注意順序)
        skin_mask = cv2.erode(skin_mask, kernel, iterations=21)
        skin_mask = cv2.dilate(skin_mask, kernel, iterations=20)

        skin_mask = cv2.GaussianBlur(skin_mask, (3, 3), 0)
        return cv2.bitwise_and(img, img, mask=skin_mask)
    else:
        raise ValueError("BAD race !")

# 根據你給的 w, h 等比例調整高度 desu。
# w && h 則一即可。
def resize_my_motherfrucker_img(img: np.ndarray, w=None, h=None, interpolation=cv2.INTER_CUBIC):
    if  (w is None and h is None) or \
        (w != None and h != None) or \
        (w==0 or h ==0):
        raise ValueError("You are retarded!!")

    img_w, img_h = img.shape[1::-1]

    if w != None:
        img_h = int(img_h/(img_w/w))
        img_w = w
    else:
        img_w = int(img_w / (img_h / h))
        img_h = h
    return cv2.resize(img, (img_w,img_h),interpolation=cv2.INTER_CUBIC)

