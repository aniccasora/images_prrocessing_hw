import cv2
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt

# 計算 輸入進來 mat的各灰階個數。
def count_grayscale(mat : np.ndarray) -> dict:
    if type(mat) != np.ndarray:
        print("mat type error!")
        exit(-1)

    # 這是比較安全的寫法，因 shape 參數可視為 [h,w[,depth]],
    w,h = mat.shape[1::-1]

    # 準備一個 dict() 計數 : {0: 0, 1: 0, ... ... ,255: 0}
    gray_value_counter = {i:0 for i in range(256)}

    # 走訪 整個 mat，其gray value 就是 gray_value_counter 的 key，我將它 +1 。
    for y in range(h):
        for x in range(w):
            gray_value_counter[mat[y][x]] += 1

    return gray_value_counter

if __name__=="__main__":
    # 檢查資料夾是否存在
    if not Path('./images').exists():
        raise Exception("\'images\' folder NOT exists !!")

    # 圖片儲存的路徑
    images_path = Path('./images').glob('*.*')

    # 將該資料夾(./images)下的 所有檔案名 存至 (list) images_path_list
    images_path_list = [str(image_path) for image_path in images_path]

    # 紀錄 img name
    img_name_list = [pth[pth.rfind('\\') + 1:] for pth in images_path_list]

    # zip (檔名 && 路徑)
    img_zip = list(zip(img_name_list, images_path_list))

    # 處理 images 內的所有圖片
    for i in range(len(img_zip)):
        # 檔名   ,對應路徑
        img_name, img_pth = img_zip[i]

        # opencv 讀取的 color 順序是BGR，在 matplotlib 內適用 RGB。
        image_bgr = cv2.imread(img_pth, cv2.IMREAD_COLOR)
        image_rgb = cv2.cvtColor(image_bgr, cv2.COLOR_BGR2RGB)
        image_gray = cv2.cvtColor(image_bgr,cv2.COLOR_BGR2GRAY)
        # ----------------------
        # 準備 figure window
        fig = plt.figure(img_name, figsize=(20, 5))
        # 放入原圖
        fig.add_subplot(131)
        plt.title(img_name + " [rgb]")
        plt.imshow(image_rgb)
        #----------------------
        # 放入 灰階圖
        fig.add_subplot(132)
        plt.title(img_name + " [gray]")
        # cmap 為設置繪圖時所用的色彩空間，show grayscale 時必須指定色彩空間 cmap='gray'。
        plt.imshow(image_gray,cmap='gray',interpolation='none')
        # ----------------------
        # 自己算 grayscale counts
        gray_count = count_grayscale(image_gray)
        # 已知 grayscale value [0~255]  x軸，
        gray_value = [i for i in range(256)]
        # 取得其各 y軸值(數量)
        counts = [gray_count[i] for i in range(256)]

        fig.add_subplot(133)
        plt.xlabel("grayscale value")
        plt.ylabel("amount")
        plt.bar(gray_value, counts)
    plt.show()
