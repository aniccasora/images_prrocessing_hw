import util as ut
import cv2
import numpy as np
import math
import matplotlib.pyplot as plt

def getSimilarRegionCoordinate(img:np.ndarray):

    w, h = img.shape[1::-1]

    # 灰階數值(dict) 累積分布
    dict_acc = ut.get_accumulation_grayscale(img)

    # 設定子區域的大小。
    scale_ratio = 0.25
    sub_ret_w,sub_ret_h = math.floor(img.shape[1]*scale_ratio),\
                          math.floor(img.shape[0]*scale_ratio)

    # 透過方框的四個角落，來指定 累積分布圖(dict_acc)的數值。
    # ∀sub_region_histogram = dict_acc([右下] - [右上] - [左下] + [左上])

    # 矩陣座標
    coordi_xy = []
    # sub region historgram，為一個 list，內部存放 dict.
    subregion_histogram_list : list([dict]) = []

    print("計算 所有 subregion 的 historgram...")
    total = (h-sub_ret_h) * (w-sub_ret_w)
    cnt = 0

    # print("dict_acc H=",len(dict_acc))
    # print("dict_acc W=", len(dict_acc[0]))
    for i in range(h-sub_ret_h):
        for j in range(w-sub_ret_w):
            print( "{:2.2f}%".format((cnt/total)*100),end ="")
            cnt += 1
            # 各區域 historgram，透過累積分布 查表找出。
            left_up_acc = dict_acc[i-1][j-1] if i!=0 and j!=0 else {i:0 for i in range(256)}
            left_bottom_acc = dict_acc[i+sub_ret_h][j-1] if j!=0 else {i:0 for i in range(256)}
            right_up_acc = dict_acc[i-1][j+sub_ret_w] if i!=0 else {i:0 for i in range(256)}
            right_buttom_acc = dict_acc[i + sub_ret_h][j + sub_ret_w]

            # ([右下] - [右上] - [左下] + [左上])
            # 由於是 dict，所以要用另外寫的函式
            sub_historgram = ut.add_dict(\
                                ut.sub_dict(\
                                    ut.sub_dict(\
                                        right_buttom_acc,\
                                        right_up_acc),\
                                    left_bottom_acc),\
                                left_up_acc)

            subregion_histogram_list.append(sub_historgram)
            coordi_xy.append((i, j))
            print("\r",end="")
    print("計算完成")

    # 計算 每個 subregion 與 global 的差距

    # 全域的 histotram 的 values list。
    g = np.array(list(ut.count_grayscale(img).values()))

    # 記錄著每一個 sub region 與 全域的 histotram 的 Mean squared error。整數
    MSE_list: list([int]) = []

    # 走訪所有 子histogram
    print("走訪所有 子histogram 計算 與 global histogram 的 MSE...")

    cnt = 0
    for histogram_dict in subregion_histogram_list:
        print("{:2.2f}%".format((cnt / total) * 100), end="")
        cnt += 1
        # 計算 MSE
        MSE_list.append(
            #  總和 (element-wise平方(element-wise相減))
            np.sum(np.square(   np.array(list(histogram_dict.values()))
                              - g
                            )
                  )
        )
        print("\r", end="")
    print("計算完成")
    # ----------------------------------

    # 從 1d-array 中, 找出 最小數值 的 idx。
    min_idx = MSE_list.index(min(MSE_list))
    # 找回 矩陣座標
    x,y = coordi_xy[min_idx][1],coordi_xy[min_idx][0]
    # 印出座標 矩陣表示法
    # print(y,x)

    # # 框出 sub image 的方框參數，可以是彩色的，但是灰階看不出來就是了。
    # rectangle_color = (255,255,255)
    # # 框線厚度。
    # thickness = 2
    # sub 矩形參數
    top_left = (x, y)
    right_bottom = (x + sub_ret_w, y + sub_ret_h)

    # subregion = img[y:y+sub_ret_h,x:x+sub_ret_w]

    # marked = cv2.rectangle(np.ndarray.copy(img), top_left, right_bottom, rectangle_color, thickness)

    print("return coordinate of subregion LT={},RB={}".format(top_left,right_bottom))
    return (x, y, sub_ret_w, sub_ret_h)