import ninjacode as nj
import numpy as np
# ===================================
# 不要管這裡面的變數，只是測試一下忍者代碼的寫法。
# 目的只是 印出 list 的資料 debug用。
# super.ninja
def print2Darray(_ : list):
    __ = len(_)
    print('[',end="")
    for idx,i in enumerate(range(__-nj.___)):
        print("",_[i],end="") if idx == nj.____ else print(" ",_[i],end=""),print(",",end="\n")
    print(" ",_[-nj.___],end=" ]\n")
# ===================================
# 取得一個 list 版的 2d array。
def get2Darray(w: int,h: int):
    return [[0 for j in range(w)] for i in range(h)]
# ===================================
# 計算 輸入進來 mat的 各灰階個數。
# @PARAM mat : numpy.ndarray
# @RETURN -> dict : 字典類型，存放 hash table 像是 {0: 2, 1:88, ... , 254: 9, 255: 1}， 記錄著灰階值數量。
def count_grayscale(mat : np.ndarray) -> dict:
    if type(mat) != np.ndarray:
        print("mat type error!")
        exit(-1)
    if len(mat.shape) > 2:
        print("non-single channel images")
        exit(-1)

    # 這是比較 generalization 的寫法，因 shape 參數可視為 [h,w[,depth]],
    w,h = mat.shape[1::-1]

    # 初始化 dict() 計數 : {0: 0, 1: 0, ... ... ,255: 0}
    grayscale_counter = {i:0 for i in range(256)}

    # 走訪 整個 mat，其gray value 就是 gray_value_counter 的 key，將它 +1 。
    for y in range(h):
        for x in range(w):
            grayscale_counter[mat[y][x]] += 1

    return grayscale_counter
# ===================================
# 字典"元素"相減
def sub_dict(a:dict, b:dict):
    if len(a) != len(b):
        print("dict len dismatch!")
        exit(-1)
    a_val = list(a.values())
    b_val = list(b.values())

    a_arr = np.array(a_val)
    b_arr = np.array(b_val)

    return dict(zip([i for i in range(256)], a_arr-b_arr))
# ===================================
# 字典"元素"相加
def add_dict(a:dict, b:dict):
    if len(a) != len(b):
        print("dict len dismatch!")
        exit(-1)

    a_val = list(a.values())
    b_val = list(b.values())

    a_arr = np.array(a_val)
    b_arr = np.array(b_val)

    return dict(zip([i for i in range(256)], a_arr+b_arr))
# ===================================
# 計算灰階的 累積分布
def get_accumulation_grayscale(img: np.array):
    w,h = img.shape[1::-1]
    # 總 pixel 數。
    total = w*h
    # 計算完成度用。
    cnt = 0
    accumulation_grayscale = get2Darray(w, h)
    for i in range(h):
        for j in range(w):
            print("生成 dict 陣列... {:2.2f} %".format((cnt / total) * 100), end="")
            accumulation_grayscale[i][j] = {i:0 for i in range(256)}
            cnt += 1
            print("\r", end="") if cnt != total else print("\n完成!!")

    cnt = 0
    for i in range(h):
        for j in range(w):
            print( "計算 灰階累積分布 {:2.2f} %".format((cnt/total) *100), end="")
            if i == j and i == 0 and j == 0:
                accumulation_grayscale[i][j] = count_grayscale(img[0:1,0:1])
            else:
                if i == 0:
                    accumulation_grayscale[0][j][img[0][j]] += 1
                    accumulation_grayscale[0][j] = add_dict(accumulation_grayscale[0][j],
                                                              accumulation_grayscale[0][j - 1])
                elif j == 0:
                    accumulation_grayscale[i][0][img[i][0]] += 1
                    accumulation_grayscale[i][0] = add_dict(accumulation_grayscale[i][0],
                                                              accumulation_grayscale[i-1][0])
                else:
                    accumulation_grayscale[i][j][img[i][j]] += 1
                    accumulation_grayscale[i][j] = add_dict(accumulation_grayscale[i][j],
                                                              accumulation_grayscale[i - 1][j - 1])
            cnt += 1
            print("\r", end="") if cnt != total else print("\n計算完成!!")
    return accumulation_grayscale