import cv2
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt
from typing import List, Tuple
import math
# 可快速計算原解析度大小的 subregion
import test.main as tm

# 子 區域 矩形比例
SUBREGION_W_H = 0.25

# 是否要 show 出 圖表。
SHOW_RESULT = True

# show opencv, debug 用
CV_SHOW = False

# 計算 輸入進來 mat的 各灰階個數。
# @PARAM mat : numpy.ndarray
# @RETURN -> dict : 字典類型，存放 hash table 像是 {0: 2, 1:88, ... , 254: 9, 255: 1}， 記錄著灰階值數量。
def count_grayscale(mat : np.ndarray) -> dict:
    if type(mat) != np.ndarray:
        print("mat type error!")
        exit(-1)
    if len(mat.shape) > 2:
        print("non-single channel images")
        exit(-1)

    # 這是比較 generalization 的寫法，因 shape 參數可視為 [h,w[,depth]],
    w,h = mat.shape[1::-1]

    # 初始化 dict() 計數 : {0: 0, 1: 0, ... ... ,255: 0}
    grayscale_counter = {i:0 for i in range(256)}

    # 走訪 整個 mat，其gray value 就是 gray_value_counter 的 key，將它 +1 。
    for y in range(h):
        for x in range(w):
            grayscale_counter[mat[y][x]] += 1

    return grayscale_counter

# 縮放圖片，照 w(pixel) 做 等比例調整，輸出後圖片寬度為 w。
# @PARAM mat: numpy.array.
# @PARAM w: 預設輸出寬度。if -1，原圖大小，這樣的算 histogram 會很久很久。
# @RETURN 縮放後的圖片, 寬度為 w。
def resizeIMG(mat,w=100):
    if w is -1:
        print("WARN : this is origin size!")
        return mat, 1
    ratio = mat.shape[1]/w
    h = int(mat.shape[0]/ratio)

    return cv2.resize(mat,(w,h)), ratio

# 決定整張圖 要用 黑色 還是 白色來 highlight.
# @PARAM : mat, 一張圖片。
# @PARAM : divider, 小於等於 視為黑，大於 視為白。
# @RETURN : (R,G,B)
def decide_use_color (mat: np.ndarray, divider = 127) -> (int,int,int):
    greyscale = list(count_grayscale(mat).values())
    white_weight = 0
    black_weight = 0
    for value in greyscale[0:divider+1]:
        black_weight += value
    for value in greyscale[divider+1:]:
        white_weight += value
    return (0,0,0) if white_weight > black_weight else (255,255,255)

# 決定整張圖 要用 多粗的框線來 highlight.
# @PARAM : mat, 一張圖片。
# @RETURN : int
def decide_use_thickness (mat: np.ndarray) -> int:
    return math.floor(max(mat.shape[1::-1])/500) + 2

if __name__=="__main__":

    # 檢查資料夾是否存在?
    if not Path('./images').exists():
        raise Exception("\'images\' folder NOT exists !!")
    if not Path('./output').exists():
        raise Exception("\'output\' folder NOT exists !!")

    # 圖片儲存的路徑。
    images_path = Path('./images').glob('*.*')

    # 將該資料夾(./images)下的 所有 "檔案名" 存至 -> images_path_list.
    images_path_list = [str(image_path) for image_path in images_path]

    # 紀錄 img name，檔名。
    img_name_list = [pth[pth.rfind('\\') + 1:] for pth in images_path_list]

    # zip (檔名 && 路徑)
    img_zip = list(zip(img_name_list, images_path_list))

    # 需要處理的圖片數
    img_amount = len(img_zip)

    # 處理 images資料夾 內的所有圖片
    for i in range(img_amount):
        # 檔名   , 對應路徑
        img_name, img_pth = img_zip[i]

        # 讀圖片，彩色的。
        origin_color_img = cv2.imread(img_pth, cv2.IMREAD_COLOR)
        # 原始大小的 灰階圖。
        origin_size_gray = cv2.imread(img_pth, cv2.IMREAD_GRAYSCALE)
        # 神奇的事情，用灰階取近來，跟 cvtColor 轉灰階 是不一樣的。!!!
        # 因為 出來的子區域是 不同的。
        # cv2.cvtColor(XX, cv2.COLOR_BGR2GRAY) != cv2.imread(XX, cv2.IMREAD_GRAYSCALE)

        # 縮小版的 彩圖。
        resize_color_img, scale_ratio = resizeIMG(origin_color_img)
        # 縮小版的 灰階。
        resized_gray_img = cv2.cvtColor(np.ndarray.copy(resize_color_img),cv2.COLOR_BGR2GRAY)

        # resize 後的圖片 長，寬
        w, h = resize_color_img.shape[1::-1]

        # 子區域的 縮放比率，最上方有定義。
        sub_ratio = SUBREGION_W_H
        # 子矩形 長寬(取上整數)。
        sub_w = math.ceil(w * sub_ratio)
        sub_h = math.ceil(h * sub_ratio)

        # 框出 sub image 的方框參數，可以是彩色的，但是灰階看不出來就是了。
        rectangle_color = decide_use_color(resized_gray_img)
        # 框線厚度。
        thickness = decide_use_thickness(origin_size_gray)

        # 子區域 方框屬性,  getSimilarRegionCoordinate 會去計算 與 全域差最小的子區域。
        sub_x, sub_y, sub_w, sub_h = tm.getSimilarRegionCoordinate(origin_size_gray)

        top_left = (sub_x, sub_y)
        right_bottom = (sub_x + sub_w, sub_y + sub_h)
        sub_img = resized_gray_img[sub_y:sub_y + sub_h, sub_x:sub_x + sub_w]

        # 切割下來的子區域
        subregion = origin_size_gray[sub_y:sub_y+sub_h,sub_x:sub_x+sub_w]

        # 這裡的方框就是已經算完的了
        marked = cv2.rectangle(np.ndarray.copy(origin_size_gray),top_left,right_bottom,rectangle_color,thickness)

        print("繪製中...", end="")
        # 接下來，畫出 histogram 的圖表, {子圖 and 整張圖}
        fig = plt.figure(num="result", figsize=(12,10))
        # 原灰圖
        fig.add_subplot(221)
        plt.title("origin grey")
        plt.imshow(cv2.cvtColor(origin_size_gray, cv2.COLOR_BGR2RGB), cmap="Greys")
        # 原灰圖 histogram
        fig.add_subplot(222)
        plt.title("global [histogram]")
        plt.xlabel("greyscale")
        plt.ylabel("times")
        # x, y 軸 數值
        plt.bar([i for i in range(256)], list(count_grayscale(origin_size_gray).values()))
        #-------------------
        # 繪出 sub region
        fig.add_subplot(223)
        plt.title("similar sub region")
        plt.imshow(cv2.cvtColor(marked, cv2.COLOR_BGR2RGB), cmap="Greys")
        # 原灰圖 histogram
        fig.add_subplot(224)
        plt.title("markup [histogram]")
        plt.xlabel("greyscale")
        plt.ylabel("times")
        # x, y 軸 數值，子圖的
        plt.bar([i for i in range(256)], list(count_grayscale(subregion).values()))

        # 必須在  plt.show() 前 先存放，因為 plt.show() 後 plt 會重新生產一個新 figure。
        output_fname = "output/output_"+ img_name[:img_name.rfind('.')]
        plt.savefig(output_fname, dpi=600, bbox_inches='tight')
        print("\r繪製完成!!")

        if SHOW_RESULT:
            print("關閉視窗繼續... waiting", end="")
            # 畫出圖表
            plt.show()
            print("\r",end="")

        plt.close("result")
        # end if, 畫圖結束
        # 已經處理完 一張 圖片，next~
