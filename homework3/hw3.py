import cv2
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
import sobel as sb

# debug 用
SHOW_PROCESS = True

# 處理，input image 資料夾。
# @RETURN : 回傳 (檔案名稱 & 路徑) 的 list.
def folder_chker():
    # 檢查資料夾是否存在
    if not Path('./images').exists():
        raise Exception("\'images\' folder NOT exists !!")
    if not Path('./output').exists():
        raise Exception("\'output\' folder NOT exists !!")

    # 圖片儲存的路徑
    images_path = Path('./images').glob('*.*')

    # 將該資料夾(./images)下的 所有檔案名 存至 (list) images_path_list
    images_path_list = [str(image_path) for image_path in images_path]

    # 紀錄 img name
    img_name_list = [pth[pth.rfind('\\') + 1:] for pth in images_path_list]

    return list(zip(img_name_list, images_path_list))

if __name__=="__main__":

    img_zip = folder_chker()

    # 處理 images 內的所有圖片
    for i in range(len(img_zip)):
        # 檔名   ,對應路徑
        img_name, img_pth = img_zip[i]

        # 用 讀入灰階圖片。
        input = cv2.imread(img_pth, cv2.IMREAD_GRAYSCALE)

        # 做高斯模糊，減少雜訊。
        smooth = cv2.GaussianBlur(input, (5,5), 1)

        # 自己寫的 sobel 物件，初始化他會對smooth 做 sobel.
        sobel_worker = sb.Mysobel(smooth)

        # 取得 sobel 後的 image，詳細實作請參考，Mysobel.__do_sobel(self) (sobel.py 檔案內)
        after_sobel_img =  sobel_worker.output

        # 做線姓疊加，權重都是 0.5
        mix_img = cv2.addWeighted(input, 0.5, after_sobel_img,0.5, 0)

        # 將結果 (兩張圖片)，放一同一個 figure。
        fig = plt.figure(num="homework3", figsize=(10,5))
        # 設定印出位置,表示 1x2 矩陣的 第 1 個元素
        fig.add_subplot(121)
        plt.title("input")
        # 關閉 座標軸刻度
        plt.xticks([]),plt.yticks([])
        # matplotlib 預設用 RGB 顯示，不轉換的話，顏色會異常
        plt.imshow(cv2.cvtColor(input, cv2.COLOR_BGR2RGB),cmap="Greys")

        # 設定印出位置,表示 1x2 矩陣的 第 2 個元素
        fig.add_subplot(122)
        plt.title("sobel mix")
        # 關閉 座標軸刻度
        plt.xticks([]), plt.yticks([])
        # matplotlib 預設用 RGB 顯示，不轉換的話，顏色會異常
        plt.imshow(cv2.cvtColor(mix_img, cv2.COLOR_BGR2RGB), cmap="Greys")

        # 寫出至檔案
        output_fname = "output/output_" + img_name
        plt.savefig(output_fname, dpi=600, bbox_inches='tight')
        plt.show()
        plt.close("homework3")
