import numpy as np

class Mysobel:
    def __init__(self, img):
        # ====================== 參數
        self.img = img
        self.__pad_img = None
        self.__filter_x = np.array(  [[-1, 0, 1],
                                      [-2, 0, 2],
                                      [-1, 0, 1]])
        self.__filter_y = np.array([[ 1,  2,  1],
                                    [ 0,  0,  0],
                                    [-1, -2, -1]])
        self.sobelx_res = []
        self.sobely_res = []
        self.sobelxy_res = []
        self.threshold = 127 # 可變動，輸出 sobel 用的
        self.output = None
        # ====================== sobel 操作
        # self.showImage()
        self.__padding_img()
        self.__do_sobel()

    # 一次將,x & y 方向都做完。
    def __do_sobel(self):
        # 走訪 padding 的 原圖(img)
        for i in range(self.__pad_img.shape[0] - 2):
            for j in range(self.__pad_img.shape[1] - 2):
                # 做 convolution，只是轉到 1維運算。  # Sobel operator 為 3*3。
                self.sobelx_res.append(np.sum(np.multiply(self.__pad_img[i:i+3, j:j+3].ravel(), self.__filter_x.ravel())))
                self.sobely_res.append(np.sum(np.multiply(self.__pad_img[i:i+3, j:j+3].ravel(), self.__filter_y.ravel())))
        # 算完的梯度會發生 負值，故這邊用 平方根(先平方再開根號)，這邊都是 element-wise計算。
        # 同時將 x y 分量的 sobel結果作結合。
        self.sobelxy_res = np.sqrt(
            np.add(
                np.square(np.array(self.sobelx_res)),
                np.square(np.array(self.sobely_res))
            )
        )

        # 因為 梯度值會有超過 255 的可能，所以這邊要做mapping，轉為 [0~255]
        for idx in range(len(self.sobelxy_res)):
            e = self.sobelxy_res[idx]
            e = 0 if e < self.threshold else 255

        self.output = np.array(self.sobelxy_res.copy() ,dtype=np.uint8)

        # 因為 2d 矩陣 convolution 是存放在 1D list,故最後需要轉換至 原本輸入圖片(img)的大小。
        self.output = self.output.reshape(self.img.shape[:2])

    # padding 原圖，因為是用 sobel opeartor，故補一圈即可，symmetric方式補邊。
    # EX: symmetric => 123 -> 321_123_321, pad_width = 3,3
    def __padding_img(self):
        self.__pad_img = np.pad(array=self.img,
                                pad_width=((1, 1), (1, 1)),
                                mode='symmetric')
