import cv2
import numpy as np
from pathlib import Path
from matplotlib import pyplot as plt

USE_CV_SHOW = False

# 有時候 圖太大了，叫這個func 可以show 小小的在旁邊
def show_img(winname,mat: np.ndarray,size=[400,-1] ):
    w, h = mat.shape[1::-1]
    size[1] = int(h/(w/size[0]))
    my_cv2_imshow(winname, cv2.resize(mat,tuple(size)))

# 取得小一點的 img size，算比例得出
def get_small_sizes(mat: np.ndarray):
    size_msg = tuple(mat.shape[1::-1])
    big_side = max(size_msg)

    resize_ratio = (big_side / 800) if big_side > 1000 else 1

    # sW, sH =
    return int(size_msg[0] / resize_ratio), int(size_msg[1] / resize_ratio)

# 決定是否用 cv2.imshow
def my_cv2_imshow(winname, mat, brbr=[],use_cvshow = USE_CV_SHOW):
    if USE_CV_SHOW and len(brbr) == 0:
        cv2.imshow(winname, mat)
    else:
        # remind USE_CV_SHOW
        pass

if __name__ == "__main__":

    images_path = Path("./images")
    for image_path in images_path.glob('*.*'):

        img_name = str(image_path)[str(image_path).rfind('\\') + 1:]
        img = cv2.imread(str(image_path), cv2.IMREAD_GRAYSCALE)
        if img is None:
            print("'{fn}'".format(fn=image_path), "is None!")
            continue

        # 看原圖
        show_img("origin",img)

        # 去躁
        n_img = cv2.GaussianBlur(img,(7,7), 0)

        # adaptive histogram equalization
        clahe = cv2.createCLAHE(clipLimit=20.0, tileGridSize=(20, 20))
        after_adp_eq = clahe.apply(n_img)

        #small 經過 adaptive histogram equalization processing...
        small = cv2.resize(after_adp_eq, get_small_sizes(after_adp_eq))
        my_cv2_imshow('After adaptive histogram equalization', small)

        ret, the = cv2.threshold(small, 135, 255, cv2.THRESH_BINARY)
        my_cv2_imshow('Binarization', the)
        cv2.waitKey(0)

        # -------------------- <用 PIL show 圖>
        fig = plt.figure(img_name,figsize=(18,5))

        fig.add_subplot(131)
        plt.title(img_name + " [origin]")
        plt.imshow(cv2.cvtColor(img,cv2.COLOR_BGR2RGB))

        fig.add_subplot(132)
        plt.title(img_name + " [adaptive histogram eq]")
        plt.imshow(after_adp_eq, cmap="gray")

        fig.add_subplot(133)
        plt.title(img_name + " [Binarization]")
        plt.imshow(cv2.cvtColor(the,cv2.COLOR_BGR2RGB))
        # output folder MUST exists!!
        plt.savefig('./output/'+img_name)
        # -------------------- </用 PIL show 圖>
    plt.show()
    cv2.destroyAllWindows()




